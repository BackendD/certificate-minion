#!/bin/bash

sed -i "s/_NAME/_acme-challenge.$CERTBOT_DOMAIN/g" arvan-new-dns-record.json;
sed -i "s/_VALUE/$CERTBOT_VALIDATION/g" arvan-new-dns-record.json;

curl -s -X POST \
    --header "Authorization: Apikey $ARVAN_API_KEY" \
    --header "Content-Type: application/json" \
    --data @arvan-new-dns-record.json \
    https://napi.arvancloud.com/cdn/4.0/domains/$CERTBOT_DOMAIN/dns-records;

sleep 31;