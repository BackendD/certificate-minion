#!/bin/bash

DNS_ID=$(curl -s --header "Authorization: Apikey $ARVAN_API_KEY" \
    https://napi.arvancloud.com/cdn/4.0/domains/$CERTBOT_DOMAIN/dns-records \
    | sed 's/","type":"txt","name":"_acme-challenge".*//' \
    | sed 's/.*"id"\:"//');

curl -s -X DELETE \
    --header "Authorization: Apikey $ARVAN_API_KEY" \
    https://napi.arvancloud.com/cdn/4.0/domains/$CERTBOT_DOMAIN/dns-records/$DNS_ID;